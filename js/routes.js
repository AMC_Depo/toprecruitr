angular.module('app.routes', [])

.config(function($stateProvider, $urlRouterProvider) {

  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider
    
      
        
    .state('tabsController.prospects', {
      url: '/page2',
      views: {
        'tab1': {
          templateUrl: 'templates/prospects.html',
          controller: 'prospectsCtrl'
        }
      }
    })
        
      
    
      
        
    .state('tabsController.meetings', {
      url: '/page3',
      views: {
        'tab2': {
          templateUrl: 'templates/meetings.html',
          controller: 'meetingsCtrl'
        }
      }
    })
        
      
    
      
        
    .state('tabsController.marketing', {
      url: '/page4',
      views: {
        'tab3': {
          templateUrl: 'templates/marketing.html',
          controller: 'marketingCtrl'
        }
      }
    })
        
      
    
      
    .state('tabsController', {
      url: '/page1',
      abstract:true,
      templateUrl: 'templates/tabsController.html'
    })
      
    
      
        
    .state('tabsController.metrics', {
      url: '/page5',
      views: {
        'tab4': {
          templateUrl: 'templates/metrics.html',
          controller: 'metricsCtrl'
        }
      }
    })
        
      
    
      
        
    .state('tabsController.quickAdd', {
      url: '/page6',
      views: {
        'tab5': {
          templateUrl: 'templates/quickAdd.html',
          controller: 'quickAddCtrl'
        }
      }
    })
        
      
    
      
        
    .state('login', {
      url: '/page7',
      templateUrl: 'templates/login.html',
      controller: 'loginCtrl'
    })
        
      
    
      
        
    .state('signup', {
      url: '/page8',
      templateUrl: 'templates/signup.html',
      controller: 'signupCtrl'
    })
        
      
    ;

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/page1/page2');

});